import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
  name: 'defaultSrcImg'
})
export class DefaultSrcImgPipe implements PipeTransform {

  transform(value: string, fallback?: string): string {
    return value ? value : fallback;
  }
}
