import { Component, OnInit, OnDestroy } from '@angular/core';
import { EventService } from '../event.service';
import { Subscribable, Subscriber, Subscription } from 'rxjs';
import { ActivatedRoute, Router } from '@angular/router';

@Component({
  selector: 'cgk-event-details',
  templateUrl: './event-details.component.html',
  styleUrls: ['./event-details.component.css']
})
export class EventDetailsComponent implements OnInit, OnDestroy {
  event: any;
  private event$: Subscription;

  constructor(
    private eventService: EventService,
    private activedRoute: ActivatedRoute,
    private router: Router
  ) {}

  ngOnInit() {
    // 1
    // this.event$ = this.eventService
    //   .get(+this.activedRoute.snapshot.params['id'])
    //   .subscribe(data => {
    //     this.event = data;
    //   });

    // 2 - better
    this.activedRoute.params.subscribe(param => {
      console.log(param);
      this.event$ = this.eventService.get(+param.id).subscribe(data => {
        this.event = data;
      });
    });
  }

  ngOnDestroy() {
    this.event$.unsubscribe();
  }

  redirectToHome() {
    // 1
    // this.router.navigate(['events']);

    // 2
    const partFooLink = 'foo';
    this.router.navigate(['events', partFooLink, 'boo']);
  }
}
