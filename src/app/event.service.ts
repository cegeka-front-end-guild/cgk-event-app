import { Injectable } from '@angular/core';
import { of, Observable, throwError } from 'rxjs';
import { catchError, delay, tap } from 'rxjs/operators';
import { HttpClient, HttpErrorResponse } from '@angular/common/http';
import { UserService } from './user/user.service';

@Injectable({
  providedIn: 'root'
})
export class EventService {
  static baseUrl = 'http://localhost:3000';

  constructor(private httpClient: HttpClient, private userService: UserService) {}

  getAll(): Observable<Event[]> {
    return of(events);
  }

  getSessions(eventId: number): Observable<any[]> {
    const data = of([].concat(...events.filter(x => x.id === eventId).map(x => x.sessions)));
    return data.pipe(delay(4000));
  }

  getAllAsPromise(): any {
    return of(events).toPromise();
  }

  getAllAsPromise2(): any {
    return Promise.resolve(events);
  }

  getAllAsPromiseWithDelay(delayMs: number): any {
    return new Promise((resolve, reject) => {
      setTimeout(() => resolve(events), delayMs);
    });
  }

  get(id: number): Observable<Event> {
    return of(events.find(x => x.id === id));
  }

  getEventsWithPromise() {
    return this.httpClient
      .get(`${EventService.baseUrl}/events`)
      .toPromise()
      .then(data => {
        return data;
      })
      .catch(error => {
        this.handleError(error);
        return Promise.reject(error);
      });
  }

  getEventsWithObserver() {
    if (!this.userService.isLoggedIn()) {
      return throwError('Not logged in');
    }

    return this.httpClient
      .get(`${EventService.baseUrl}/events`)
      .pipe(catchError(err => this.handleErrors(err)));
  }

  handleError(error) {
    if (error.status === 401) {
      console.error('You don`t have permission');
    } else {
      console.error(`Houston, we got a problem.${error}`);
    }
  }

  handleErrors(error: HttpErrorResponse) {
    if (error.error instanceof ErrorEvent) {
      console.error(
        'A client-side or network error occurred:',
        error.error.message
      );
    } else {
      console.error(
        `Backend returned code ${error.status}, ` + `body was: ${error.error}`
      );
    }

    return throwError(
      'Something bad happened; please try again later.'
    );
  }
}

export interface Event {
  id: number;
  name: string;
  date: string;
  time: string;
  price: any;
  location?: Location;
  sessions: Session[];
}

interface Location {
  address: string;
  city: string;
  country: string;
}

interface Session {
  id: number;
  name: string;
  presenter: string;
  duration: number;
  level: string;
  abstract: string;
}

// https://mockaroo.com/
// tslint:disable:max-line-length
const events = [
  {
    id: 1,
    name: 'Crotalus triseriatus',
    date: '8/2/2016',
    time: '9:49 PM',
    price: 30.31,
    location: {
      address: '1057 DT',
      city: 'London',
      country: 'England'
    },
    sessions: [
      {
        id: 1,
        name: 'Using Angular 4 Pipes',
        presenter: 'Peter Bacon Darwin',
        duration: 1,
        level: 'Intermediate',
        abstract:
          'Learn all about the new pipes in Angular 4, both\n          how to write them, and how to get the new AI CLI to write\n          them for you. Given by the famous PBD, president of Angular\n          University (formerly Oxford University)'
      },
      {
        id: 2,
        name: 'Getting the most out of your dev team',
        presenter: 'Jeff Cross',
        duration: 1,
        level: 'Intermediate',
        abstract:
          'We all know that our dev teams work hard, but with\n          the right management they can be even more productive, without\n          overworking them. In this session I\'ll show you how to get the\n          best results from the talent you already have on staff.'
      }
    ],
    onlineUrl:
      'http://dell.com/eget/tincidunt/eget.xml?orci=sit&eget=amet&orci=consectetuer&vehicula=adipiscing&condimentum=elit&curabitur=proin&in=risus&libero=praesent&ut=lectus&massa=vestibulum&volutpat=quam&convallis=sapien&morbi=varius&odio=ut&odio=blandit&elementum=non&eu=interdum&interdum=in&eu=ante&tincidunt=vestibulum&in=ante&leo=ipsum&maecenas=primis&pulvinar=in&lobortis=faucibus&est=orci&phasellus=luctus&sit=et&amet=ultrices&erat=posuere&nulla=cubilia&tempus=curae&vivamus=duis&in=faucibus&felis=accumsan&eu=odio&sapien=curabitur&cursus=convallis&vestibulum=duis&proin=consequat&eu=dui&mi=nec&nulla=nisi&ac=volutpat&enim=eleifend&in=donec&tempor=ut&turpis=dolor&nec=morbi&euismod=vel&scelerisque=lectus&quam=in&turpis=quam&adipiscing=fringilla&lorem=rhoncus&vitae=mauris&mattis=enim&nibh=leo&ligula=rhoncus&nec=sed&sem=vestibulum&duis=sit&aliquam=amet&convallis=cursus&nunc=id&proin=turpis&at=integer&turpis=aliquet&a=massa&pede=id&posuere=lobortis&nonummy=convallis&integer=tortor&non=risus&velit=dapibus&donec=augue&diam=vel&neque=accumsan&vestibulum=tellus&eget=nisi&vulputate=eu&ut=orci&ultrices=mauris&vel=lacinia&augue=sapien&vestibulum=quis&ante=libero&ipsum=nullam&primis=sit&in=amet&faucibus=turpis&orci=elementum&luctus=ligula&et=vehicula&ultrices=consequat&posuere=morbi&cubilia=a&curae=ipsum&donec=integer',
    imageUrl: ''
  },
  {
    id: 2,
    name: 'Tachyglossus aculeatus',
    date: '6/25/2016',
    time: '3:33 AM',
    price: 28.21,
    sessions: [
      {
        id: 1,
        name: 'Testing Angular 4 Workshop',
        presenter: 'Pascal Precht & Christoph Bergdorf',
        duration: 4,
        level: 'Beginner',
        abstract:
          'In this 6 hour workshop you will learn not only how to test Angular 4,\n          you will also learn how to make the most of your team\'s efforts. Other topics\n          will be convincing your manager that testing is a good idea, and using the new\n          protractor tool for end to end testing.'
      },
      {
        id: 2,
        name: 'Angular 4 and Firebase',
        presenter: 'David East',
        duration: 3,
        level: 'Intermediate',
        abstract:
          'In this workshop, David East will show you how to use Angular with the new\n          ultra-real-time 5D Firebase back end, hosting platform, and wine recommendation engine.'
      }
    ],
    onlineUrl:
      'https://ftc.gov/leo/rhoncus.jpg?vulputate=eros&luctus=viverra&cum=eget&sociis=congue&natoque=eget&penatibus=semper&et=rutrum&magnis=nulla&dis=nunc&parturient=purus&montes=phasellus&nascetur=in&ridiculus=felis&mus=donec&vivamus=semper&vestibulum=sapien&sagittis=a&sapien=libero&cum=nam&sociis=dui&natoque=proin&penatibus=leo&et=odio&magnis=porttitor&dis=id&parturient=consequat&montes=in&nascetur=consequat&ridiculus=ut&mus=nulla&etiam=sed&vel=accumsan&augue=felis&vestibulum=ut&rutrum=at&rutrum=dolor&neque=quis&aenean=odio&auctor=consequat&gravida=varius&sem=integer&praesent=ac&id=leo&massa=pellentesque&id=ultrices&nisl=mattis&venenatis=odio&lacinia=donec&aenean=vitae&sit=nisi&amet=nam&justo=ultrices&morbi=libero',
    imageUrl: 'http://dummyimage.com/244x108.png/ff4444/ffffff'
  }
];
