import { Directive, OnInit, ElementRef, Renderer, HostListener } from '@angular/core';

@Directive({
  selector: '[cgkRedTextDirective]'
})
export class RedTextDirectiveDirective implements OnInit {

  @HostListener('mouseenter') onMouseEnter() {
    this.updateTextColor('green');
  }

  @HostListener('mouseleave') onMouseLeave() {
    this.updateTextColor('red');
  }

  constructor(private el: ElementRef, private renderer: Renderer) {}

  ngOnInit(): void {
    this.updateTextColor('red');
  }

  updateTextColor(color: string) {
    // 1
    // this.el.nativeElement.style.color = color;

    // 2
    this.renderer.setElementStyle(this.el.nativeElement, 'color', color);
  }
}
