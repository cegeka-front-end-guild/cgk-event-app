import { Component, OnInit } from '@angular/core';
import { Event as EventInterface } from '../event.service';

class Event implements Partial<EventInterface> {
  id: number;
  name: string;
  date: string;
  time: string;
  price: any;
}

@Component({
  selector: 'cgk-event-add',
  templateUrl: './event-add.component.html',
  styleUrls: ['./event-add.component.css']
})
export class EventAddComponent implements OnInit {

  counter = 1;
  event = new Event();

  constructor() { }

  ngOnInit() { }

  addEvent() {
  }

}
