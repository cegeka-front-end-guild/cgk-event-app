import { TestBed, inject } from '@angular/core/testing';

import { EventService } from './event.service';
import {
  HttpClientTestingModule,
  HttpTestingController
} from '@angular/common/http/testing';
import { RouterTestingModule } from '@angular/router/testing';
import { UserService } from './user/user.service';

class UserServiceMock {
  isLoggedIn() {
    return 'something';
  }
}

describe('Given an EventService', () => {
  let httpMock: HttpTestingController;
  let service: EventService;

  // 1
  // let userServiceSpy: jasmine.SpyObj<UserService>;

  // 2
  let userService: UserService;

  beforeEach(() => {
    // 1
    // const spyUserService = jasmine.createSpyObj('UserService', ['isLoggedIn']);

    TestBed.configureTestingModule({
      imports: [HttpClientTestingModule],
      providers: [EventService, {
        provide: UserService,
        useClass: UserServiceMock
        // 1. useValue: spyUserService
      }]
    });

    service = TestBed.get(EventService);
    httpMock = TestBed.get(HttpTestingController);

    // 1
    // userServiceSpy = TestBed.get(UserService);

    // 2
    userService = TestBed.get(UserService);
  });

  it('should be created (1)', () => {
    expect(service).toBeTruthy();
  });

  it('should be created (2)', inject([EventService], (serviceTemp: EventService) => {
    expect(serviceTemp).toBeTruthy();
  }));

  it('should return an Observable of 0 mocked data', () => {
    // userServiceSpy.isLoggedIn.and.returnValue(true);

    service.getEventsWithObserver().subscribe(data => {
      expect((<Event[]>data).length).toBe(0);
    });

    const req = httpMock.expectOne(
      `${EventService.baseUrl}/events`
    );
    expect(req.request.method).toBe('GET');
    req.flush('');
  });

  it('should return an Observable of 2 mocked data', inject([EventService], (service: EventService) => {
    // userServiceSpy.isLoggedIn.and.returnValue(true);

    const mockData = getTwoMockData();

    service.getEventsWithObserver().subscribe(data => {
      expect((data.events).length).toBe(2);
    });

    const req = httpMock.expectOne(
      `${EventService.baseUrl}/events`
    );
    expect(req.request.method).toBe('GET');
    req.flush(mockData);
  }));

  it('should do an http call', () => {
    // userServiceSpy.isLoggedIn.and.returnValue(true);

    service.getEventsWithObserver().subscribe(data => {
    });

    httpMock.expectOne(`${EventService.baseUrl}/events`);
  });

  it('should throw Error when user is not logged', () => {
    // userServiceSpy.isLoggedIn.and.returnValue(false);

    spyOn(userService, 'isLoggedIn').and.returnValue(false);


    service.getEventsWithObserver().subscribe(data => {}, err => {
      expect(err).toBeTruthy();
    });
  });
);

function getTwoMockData() {
  return {
    'events': [
      {}, {}
    ]
  };
}
