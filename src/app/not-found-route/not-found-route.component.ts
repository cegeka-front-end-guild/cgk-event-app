import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'cgk-not-found-route',
  templateUrl: './not-found-route.component.html',
  styleUrls: ['./not-found-route.component.css']
})
export class NotFoundRouteComponent implements OnInit {

  constructor() { }

  ngOnInit() {
  }

}
