import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { EventListComponent } from './event-list/event-list.component';
import { EventDetailsComponent } from './event-details/event-details.component';
import { NotFoundRouteComponent } from './not-found-route/not-found-route.component';
import { LoginComponent } from './user/login/login.component';
import { AlwaysAuthGuard } from './guards/always-auth-guard';
import { OnlyLoggedInUsersGuard } from './guards/only-logged-in-users-guard';
import { EventAddComponent } from './event-add/event-add.component';
import { SessionListComponent } from './session-list/session-list.component';
import { SpeakerListComponent } from './speaker-list/speaker-list.component';
import { SessionResolverService } from './session-list/session-resolver.service';

const routes: Routes = [
  { path: '', pathMatch: 'full', redirectTo: 'events' },
  { path: 'events', component: EventListComponent },
  { path: 'events/foo/boo', redirectTo: 'events' },
  { path: 'events/add', component: EventAddComponent },
  {
    path: 'events/:id',
    component: EventDetailsComponent,
    canActivate: [OnlyLoggedInUsersGuard, AlwaysAuthGuard],
    children: [
      { path: '', redirectTo: 'session', pathMatch: 'full' },
      { path: 'session', component: SessionListComponent, resolve: { 'sessions': SessionResolverService} },
      { path: 'speaker', component: SpeakerListComponent}
    ]
  },
  { path: 'user', loadChildren: './user/user.module#UserModule' },
  { path: '**', component: NotFoundRouteComponent }
];

@NgModule({
  imports: [
    RouterModule.forRoot(routes, { useHash: false, enableTracing: true })
  ],
  exports: [RouterModule]
})
export class AppRoutingModule {}
