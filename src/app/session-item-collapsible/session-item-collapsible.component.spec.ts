import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { SessionItemCollapsibleComponent } from './session-item-collapsible.component';

describe('SessionItemCollapsibleComponent', () => {
  let component: SessionItemCollapsibleComponent;
  let fixture: ComponentFixture<SessionItemCollapsibleComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SessionItemCollapsibleComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SessionItemCollapsibleComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
