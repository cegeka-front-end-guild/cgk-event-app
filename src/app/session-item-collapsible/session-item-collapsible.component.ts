import { Component, OnInit, Input } from '@angular/core';

@Component({
  selector: 'cgk-session-item-collapsible',
  templateUrl: './session-item-collapsible.component.html',
  styleUrls: ['./session-item-collapsible.component.css']
})
export class SessionItemCollapsibleComponent implements OnInit {
  visible = true;

  constructor() { }

  ngOnInit() {
  }

  toggleContent() {
    this.visible = !this.visible;
  }
}
