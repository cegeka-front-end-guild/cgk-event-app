import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { EventInfoComponent } from './event-info.component';
import { SharedModule } from '../shared/shared.module';
import { DebugElement } from '@angular/core';
import { By } from '@angular/platform-browser';

fdescribe('EventInfoComponent', () => {
  let component: EventInfoComponent;
  let fixture: ComponentFixture<EventInfoComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ EventInfoComponent ],
      imports: [SharedModule]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(EventInfoComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('should have an account icon - NativeElement', () => {
    const eventInfoElement: HTMLElement = fixture.nativeElement;
    const matIcon = eventInfoElement.querySelector('mat-icon');

    expect(matIcon.textContent).toContain('account_circle');
  });

  it('should have an account icon - Unwrapping DebugElement', () => {
    const eventInfoElement: DebugElement = fixture.debugElement;
    const eventInfoEl: HTMLElement = eventInfoElement.nativeElement;
    const matIcon = eventInfoEl.querySelector('mat-icon');

    expect(matIcon.textContent).toContain('account_circle');
  });

  it('should have an account icon - using predicate', () => {
    const eventInfoElement: DebugElement = fixture.debugElement;
    const matIconDe: DebugElement = eventInfoElement.query(By.css('mat-icon'));

    expect(matIconDe.nativeElement.textContent).toContain('account_circle');
  });

  it('should not display title in the DOM after createComponent()', () => {
    const eventInfoElTitle: HTMLElement = fixture.nativeElement.querySelector('mat-card-title');
    component.event = {name: 'dummy title'};

    expect(eventInfoElTitle.textContent).toBe('');
  });

  it('should display title in the DOM after detectChanges()', () => {
    const eventInfoElTitle: HTMLElement = fixture.nativeElement.querySelector('mat-card-title');
    component.event = {name: 'dummy title'};

    fixture.detectChanges();

    expect(eventInfoElTitle.textContent).toBe(component.event.name);
    expect(eventInfoElTitle.textContent).toBe('dummy title');
  });
});
