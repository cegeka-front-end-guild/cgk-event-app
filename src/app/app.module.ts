import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { EventListComponent } from './event-list/event-list.component';
import { EventDetailsComponent } from './event-details/event-details.component';
import { EventInfoComponent } from './event-info/event-info.component';
import { NavComponent } from './nav/nav.component';
import { LayoutModule } from '@angular/cdk/layout';

import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { NotFoundRouteComponent } from './not-found-route/not-found-route.component';
import { LoginComponent } from './user/login/login.component';
import { AlwaysAuthGuard } from './guards/always-auth-guard';
import { OnlyLoggedInUsersGuard } from './guards/only-logged-in-users-guard';
import { UserService } from './user/user.service';
import { RedTextDirectiveDirective } from './directives/red-text-directive.directive';
import { DefaultSrcImgPipe } from './pipes/default-src-img.pipe';
import { HttpClientModule } from '@angular/common/http';
import { EventAddComponent } from './event-add/event-add.component';
import { FormsModule } from '@angular/forms';
import { SharedModule } from './shared/shared.module';
import { SessionListComponent } from './session-list/session-list.component';
import { SpeakerListComponent } from './speaker-list/speaker-list.component';
import { MatProgressSpinnerModule } from '@angular/material';
import { SessionItemCollapsibleComponent } from './session-item-collapsible/session-item-collapsible.component';

@NgModule({
  declarations: [
    AppComponent,
    EventListComponent,
    EventDetailsComponent,
    EventInfoComponent,
    NavComponent,
    NotFoundRouteComponent,
    EventAddComponent,
    RedTextDirectiveDirective,
    DefaultSrcImgPipe,
    SessionListComponent,
    SpeakerListComponent,
    SessionItemCollapsibleComponent
  ],
  imports: [
    BrowserModule,
    BrowserAnimationsModule,
    FormsModule,
    AppRoutingModule,
    LayoutModule,
    SharedModule,
    HttpClientModule
  ],
  providers: [AlwaysAuthGuard, OnlyLoggedInUsersGuard],
  bootstrap: [AppComponent]
})
export class AppModule {}
