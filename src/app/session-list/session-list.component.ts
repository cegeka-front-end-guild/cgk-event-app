import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';

@Component({
  selector: 'cgk-session-list',
  templateUrl: './session-list.component.html',
  styleUrls: ['./session-list.component.css']
})
export class SessionListComponent implements OnInit {
  sessions: any[];

  constructor(private route: ActivatedRoute) { }

  ngOnInit() {
    this.sessions = this.route.snapshot.data['sessions'];
  }

}
