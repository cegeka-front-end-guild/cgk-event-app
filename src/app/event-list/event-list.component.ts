import { Component, OnInit, OnDestroy } from '@angular/core';
import { EventService } from '../event.service';

@Component({
  selector: 'cgk-event-list',
  templateUrl: './event-list.component.html',
  styleUrls: ['./event-list.component.css']
})
export class EventListComponent implements OnInit, OnDestroy {
  events$: any;
  eventsAsPromise: any;

  events: Event[];
  eventsObs: Event[];

  constructor(private eventService: EventService) {}

  ngOnInit() {
    this.events$ = this.eventService.getAll();
    this.eventsAsPromise = this.eventService.getAllAsPromiseWithDelay(3000);

    this.eventService.getEventsWithPromise().then(data => {
      this.events = <Event[]>data;
    });

    this.eventService.getEventsWithObserver().subscribe(data => {
      console.log(data);
      this.eventsObs = <Event[]>data;
    });
  }

  ngOnDestroy() {}

  onEventInfoClick() {
    console.log('info event');
  }
}
