import { NgModule } from '@angular/core';
import { CommonModule,  } from '@angular/common';
import { RouterModule } from '@angular/router';
import { userRoutes } from './user-routing';
import { LoginComponent } from './login/login.component';
import { UserProfileComponent } from './user-profile/user-profile.component';
import { ReactiveFormsModule } from '@angular/forms';
import { MatFormFieldModule, MatInputModule, MatButtonModule } from '@angular/material';
import { SharedModule } from '../shared/shared.module';

@NgModule({
  imports: [
    CommonModule,
    ReactiveFormsModule,
    RouterModule.forChild(userRoutes),
    SharedModule
  ],
  declarations: [LoginComponent, UserProfileComponent]
})
export class UserModule { }
